#include "header\labviewInterface.h"

Network labview_to_armadillo(double* NN_Weights_L1, double* NN_Weights_L2, double* input_weights, uint32_t* w_dim, uint32_t layerCount)
{
	Network NN;
	Layers L1, L2;
	uint32_t elem = 0;

	for (size_t i = 0; i <= layerCount; i++)
	{
		L1.weight = arma::mat(&(NN_Weights_L1[elem]), w_dim[2 * i], w_dim[2 * i + 1]);
		L2.weight = arma::mat(&(NN_Weights_L2[elem]), w_dim[2 * i], w_dim[2 * i + 1]);

		elem +=  w_dim[2 * i] * w_dim[2 * i + 1];

		L1.bias = arma::mat(&(NN_Weights_L1[elem]), w_dim[2 * i], 1);
		L2.bias = arma::mat(&(NN_Weights_L2[elem]), w_dim[2 * i], 1);

		NN.L1.push_back(L1);
		NN.L2.push_back(L2);
		elem +=  w_dim[2 * i] * 1;
	}

	NN.input_W.W1 = input_weights[0];
	NN.input_W.W2 = input_weights[1];
	NN.input_W.Wu = input_weights[2];
	NN.input_W.Wo = input_weights[3];
	NN.input_W.Wg = input_weights[4];
	NN.layerCount = layerCount;

	//alpha creator
	NN.L1.push_back(L1);
	NN.L2.push_back(L2);

	return NN;
}


Interface_Sizes armadillo_to_labview(Network NN, double** NN_Weights_L1, double** NN_Weights_L2, double* input_weights, uint32_t** w_dim)
{
	//Todas as matrizes de L1, depois todas as matrizes de L2, depois 4 elementos
	// W1, W2, Wo e Wu necessariamente nesta ordem.
	uint32_t size_accumulator = 0;
	uint32_t dim_accumulator = 0;
	Interface_Sizes returned;

	arma::Col<uint32_t> neurons = NN.L1[0].neurons;

	for (size_t i = 0; i <= NN.layerCount; i++)
	{
		dim_accumulator += 2;
	}
	uint32_t position_counter = 0;

	(*w_dim) = (uint32_t*) malloc(sizeof(uint32_t) * dim_accumulator);

	returned.dim_size = dim_accumulator;

	for (size_t i = 0; i <= NN.layerCount; i++)
	{
		size_accumulator += NN.L1[i].weight.n_elem;
		size_accumulator += NN.L1[i].bias.n_elem;

		(*w_dim)[position_counter++] = NN.L1[i].weight.n_rows;
		(*w_dim)[position_counter++] = NN.L1[i].weight.n_cols;
	}

	(*NN_Weights_L1) = (double*) malloc(sizeof(double) * size_accumulator);
	(*NN_Weights_L2) = (double*) malloc(sizeof(double) * size_accumulator);
	position_counter = 0;
	returned.W1_size = size_accumulator;
	for (size_t i = 0; i <= NN.layerCount; i++)
	{
		for (size_t j = 0; j < NN.L1[i].weight.n_elem; j++)
		{
			(*NN_Weights_L1)[position_counter] = NN.L1[i].weight[j];
			position_counter++;
		}
		for (size_t k = 0; k < NN.L1[i].bias.n_elem; k++)
		{
			(*NN_Weights_L1)[position_counter] = NN.L1[i].bias[k];
			position_counter++;
		}
	}
	position_counter = 0;
	for (size_t i = 0; i <= NN.layerCount; i++)
	{
		for (size_t j = 0; j < NN.L2[i].weight.n_elem; j++)
		{
			(*NN_Weights_L2)[position_counter] = NN.L2[i].weight[j];
			position_counter++;
		}
		for (size_t k = 0; k < NN.L2[i].bias.n_elem; k++)
		{
			(*NN_Weights_L2)[position_counter] = NN.L2[i].bias[k];
			position_counter++;
		}
	}
	
	input_weights[0] = NN.input_W.W1;
	input_weights[1] = NN.input_W.W2;
	input_weights[2] = NN.input_W.Wu;
	input_weights[3] = NN.input_W.Wo;
	input_weights[4] = NN.input_W.Wg;

	return returned;
}
