#include "header\resilientbackprop.h"

 
void ResilientBackProp_caloba(std::vector<Layers> &L, uint32_t layerCount, double & a_)
 {
	 using namespace arma;
	 for (int k = 0; k <= layerCount + 1; k++)
	 {
		 mat b;
		 uvec plusB = find(L[k].dB_Old % L[k].dB > 0);
		 uvec minusB = find(L[k].dB_Old % L[k].dB < 0);

		 uvec plusW = find(L[k].dW_Old % L[k].dW > 0);
		 uvec minusW = find(L[k].dW_Old % L[k].dW < 0);

		 b = L[k].dB_Old / (L[k].dB_Old - L[k].dB);

		 if (plusB.n_elem > 0 || plusW.n_elem > 0)
			 a_ = a_*A_FACTOR_RES_BP;

	//	if (a_ > MAXIMUM_A_VALUE)
	//		a_ = MAXIMUM_A_VALUE;

		 L[k].dB.elem(plusB) = L[k].dB.elem(plusB)*a_;
		 L[k].dW.elem(plusW) = L[k].dW.elem(plusW)*a_;

		 L[k].dB.elem(minusB) = L[k].dB.elem(minusB) % b.elem(minusB);

		 b = L[k].dW_Old / (L[k].dW_Old - L[k].dW);

		 L[k].dW.elem(minusW) = L[k].dW.elem(minusW) % b.elem(minusW);
	 }

 }

void iRprop_minus(std::vector<Layers>& L, uint32_t layerCount)
{

	using namespace arma;

	for (int k = 0; k <= layerCount + 1; k++)
	{
		mat b;
		uvec plusB = find(L[k].dB_Old % L[k].dB > 0);
		uvec minusB = find(L[k].dB_Old % L[k].dB < 0);
		uvec zeroB = find(L[k].dB_Old % L[k].dB == 0);

		uvec plusW = find(L[k].dW_Old % L[k].dW > 0);
		uvec minusW = find(L[k].dW_Old % L[k].dW < 0);
		uvec zeroW = find(L[k].dW_Old % L[k].dW == 0);


		// case old * now > 0
		L[k].deltaB.elem(plusB) = min(L[k].deltaB.elem(plusB)*ETA_PLUS, DELTA_MAX*arma::ones<mat>(size(L[k].deltaB.elem(plusB))));
		L[k].deltaW.elem(plusW) = min(L[k].deltaW.elem(plusW)*ETA_PLUS, DELTA_MAX*arma::ones<mat>(size(L[k].deltaW.elem(plusW))));

		//case old * now < 0
		L[k].deltaB.elem(minusB) = max(L[k].deltaB.elem(minusB)*ETA_MINUS, DELTA_MIN*arma::ones<mat>(size(L[k].deltaB.elem(minusB))));
		L[k].deltaW.elem(minusW) = max(L[k].deltaW.elem(minusW)*ETA_MINUS, DELTA_MIN*arma::ones<mat>(size(L[k].deltaW.elem(minusW))));

		L[k].dB.elem(minusB) = 0 * arma::ones<mat>(size(L[k].dB.elem(minusB)));
		L[k].dW.elem(minusW) = 0 * arma::ones<mat>(size(L[k].dW.elem(minusW)));

		//changes to dB and dW
		L[k].dB = -sign(L[k].dB) % L[k].deltaB;
		L[k].dW = -sign(L[k].dW) % L[k].deltaW;

	}


}
