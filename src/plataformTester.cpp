// plataformTester.cpp : Defines the entry point for the console application.
#include "header\stdafx.h"

#include <armadillo>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>

#include "header\gnuplot-iostream-master\gnuplot-iostream.h"
#include "header\fileManager.h"
#include "header\nngenerator.h"
#include "header\trainResBPNarma.h"
#include "header\labviewInterface.h"


double operateControl(double* weights_L1, double* weights_L2, double* input_W_test, uint32_t* dimensions, uint32_t layerCount,
	double d_output_error, double* last_elements, double* input_labview, double target, uint32_t input_delay, uint32_t output_delay)
{
	Network NN;
	double* dummy_w1 = NULL;
	double* dummy_w2 = NULL;
	uint32_t* dummy_dim = NULL;
	Interface_Sizes labview_it;
	double output_control;
	arma::mat input_arma(input_labview, input_delay + output_delay - 1, 1);

	double last_g = last_elements[0];
	double last_f = last_elements[1];
	double last_ref = last_elements[2];


	NN = labview_to_armadillo(weights_L1, weights_L2, input_W_test, dimensions, layerCount);

	output_control = trainResBPNarmaCONTROL(NN,
		layerCount,
		d_output_error,
		last_g, last_f, last_ref,
		input_arma, target);


	labview_it = armadillo_to_labview(NN, &dummy_w1, &dummy_w2, input_W_test, &dummy_dim);


	for (size_t i = 0; i < labview_it.W1_size; i++)
	{
		weights_L1[i] = dummy_w1[i];
		weights_L2[i] = dummy_w2[i];
	}

	for (size_t i = 0; i < labview_it.dim_size; i++)
	{
		dimensions[i] = dummy_dim[i];
	}

	return output_control;
}


int main()
{
	Gnuplot gp,gp_;

	Gnuplot gp_meudeus, gp_blablabla;

	std::string filePath;
	arma::mat input_Armadillo, output_Armadillo;
	double span = SPAN;
	uint32_t layerCount = 1;

	std::vector<Layers> L1, L2;
	input_weights i_W;
	arma::mat aux;

	uint32_t neurons[1];
	neurons[0] = 9;

	// extracts input and output from file
	filePath = "I:\\BitBucket\\projetofinal\\CollectedData\\New\\Relay\\CXX2609ESnvhResults.txt";
	readFileNew(filePath, input_Armadillo, output_Armadillo, 2);

	//
	//double A = arma::mean(arma::mean(input_Armadillo, 0));
	//std::cout << A << '\n';
	//double B = arma::stddev(input_Armadillo.col(0), 0);
	//std::cout << B << '\n';

	createDataSets(input_Armadillo, output_Armadillo, 3, 2);
	
	// format -> inputs x dataSize
	input_Armadillo = input_Armadillo.t();
	output_Armadillo = output_Armadillo.t();

	arma::mat aux_ = input_Armadillo.row(0);

	gp_meudeus << "plot '-' with lines title 'first input' \n";
	gp_meudeus.send(aux_);
	aux_ = input_Armadillo.row(input_Armadillo.n_rows-1);

	gp_blablabla << "plot '-' with lines title 'last input' \n";
	gp_blablabla.send(aux_);

	// MLP neural net creator (L1, L2)
	L1 = NNGenerator(neurons, layerCount, span, input_Armadillo, output_Armadillo);
	L2 = NNGenerator(neurons, layerCount, span, input_Armadillo, output_Armadillo);

	//structure that holds the other input weights
	aux.randu(1, 4);
	aux = -span + 2 *span* aux;
	i_W.W1 = aux[0];
	i_W.W2 = aux[1];
	i_W.Wo = aux[2];
	i_W.Wu = aux[3];

	//structure that holds entire network
	Network neural_network_NARMA;

	neural_network_NARMA.layerCount = layerCount;

	neural_network_NARMA.L1 = L1;
	neural_network_NARMA.L2 = L2;
	neural_network_NARMA.input_W = i_W;

	uint32_t runs = 250;

	neural_network_NARMA.it_cost.J_train.reserve(runs);
	neural_network_NARMA.it_cost.J_val.reserve(runs);
	neural_network_NARMA.it_cost.J_test.reserve(runs);

	//training
	neural_network_NARMA = trainResBPNARMAL2(neural_network_NARMA,
		1,
		input_Armadillo, output_Armadillo,
		runs);

	gp << "plot '-' with lines title 'ref', '-' with lines title 'nn output' \n";
	gp.send(output_Armadillo);
	gp.send(neural_network_NARMA.trainedOutput);

	gp_ << "plot '-' with lines title 'Train', '-' with lines title 'Val', '-' with lines title 'Test' \n";
	gp_.send(neural_network_NARMA.it_cost.J_train);
	gp_.send(neural_network_NARMA.it_cost.J_val);
	gp_.send(neural_network_NARMA.it_cost.J_test);

	std::cin.get();

	std::ofstream FILE("file_output_costs.txt");

	for (size_t i = 0; i < neural_network_NARMA.it_cost.J_train.size(); i++)
	{
		FILE << neural_network_NARMA.it_cost.J_train[i]
			<< "," << neural_network_NARMA.it_cost.J_val[i]
			<< "," << neural_network_NARMA.it_cost.J_test[i] << "\n";
	}
	input_Armadillo.save("input_file.dat", arma::raw_ascii);
	output_Armadillo.save("output_file.dat", arma::raw_ascii);

	Network test_net;
	double* weights_L1 = NULL;
	double* weights_L2 = NULL;
	uint32_t* dimensions = NULL;

	double* input_W_test;

	input_W_test = (double*)malloc(sizeof(double) * 5);


	armadillo_to_labview(neural_network_NARMA, &weights_L1, &weights_L2, input_W_test, &dimensions);
	std::cout << weights_L1[35] << '\n';
	test_net = labview_to_armadillo(weights_L1, weights_L2, input_W_test, dimensions, 1);

	//std::cout << neural_network_NARMA.L1[0].weight << '\n';
	//std::cout << "------------------------" << '\n';
	//std::cout << test_net.L1[0].weight << '\n';

	//std::cout << neural_network_NARMA.L1[1].weight << '\n';
	//std::cout << "------------------------" << '\n';
	//std::cout << test_net.L1[1].weight << '\n';


	//std::cout << neural_network_NARMA.L1[0].bias << '\n';
	//std::cout << "------------------------" << '\n';
	//std::cout << test_net.L1[0].bias << '\n';

	//std::cout << neural_network_NARMA.L1[1].bias << '\n';
	//std::cout << "------------------------" << '\n';
	//std::cout << test_net.L1[1].bias << '\n';

	double last_elements[3];
	last_elements[0] = 1;
	last_elements[1] = 0;
	last_elements[2] = 0;

	double input_labview[4];
	input_labview[0] = 10;
	input_labview[1] = 5;
	input_labview[2] = -5;
	input_labview[3] = 25;

	double input_CORRENTE = operateControl(weights_L1, weights_L2, input_W_test, dimensions, layerCount,
		0, last_elements, input_labview, 20.0, 3, 2);

	std::cout << input_CORRENTE << std::endl;

    return 0;
}