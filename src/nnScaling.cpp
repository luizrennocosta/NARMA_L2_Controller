#include "header\nnScaling.h"

void nnInputScaling(std::vector<Layers>& L, std::vector<double> means, std::vector<double> stds)
{
	//scaling do bias
	for (size_t i = 0; i < L[0].bias.n_elem-1; i++)
	{
		for (size_t j = 0; j < means.size()-2; j++)
		{
			L[0].bias[i] -= L[0].weight.at(i, j) * means[j]/stds[j];
		}
	}
	//scaling do weight
	for (size_t i = 0; i < L[0].weight.n_cols-1; i++)
	{
		L[0].weight.col(i) /= stds[i];
	}
}

void narmaNetworkScaling(Network& narma, std::vector<double> means, std::vector<double> stds)
{
	nnInputScaling(narma.L1, means,stds);
	nnInputScaling(narma.L2, means, stds);

	narma.input_W.Wo *= stds.back()/stds[0];
	narma.input_W.W2 *= stds.back();
}
