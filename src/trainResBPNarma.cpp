#include "header\trainResBPNarma.h"
#include <iostream>

#define NN_TRAIN


Network trainResBPNARMAL2(Network NN,
	uint32_t layerCount,
	arma::mat input, arma::mat target, 
	uint32_t runs) 
{
	using namespace arma;
	Network trained_network;

	NN.it_cost.J_train.reserve(runs);
	NN.it_cost.J_val.reserve(runs);
	NN.it_cost.J_test.reserve(runs);

	double Jmin = 100000;

    mat output1,output2,z1,z2,z3, y_final;
    mat err;

	uint32_t i = 0;
	bool begin = true;

    double a_ = 1.1;
    double input_data_size = input.n_cols;

    uint32_t layerCount_1 = layerCount;
    uint32_t layerCount_2 = layerCount;

	int trainSetSize = (int)input_data_size * TRAIN_PERCENTAGE_AMOUNT;
	int valSetSize   = (int)input_data_size * VALIDATION_PERCENTAGE_AMOUNT;
	int testSetSize  = (int)input_data_size * TEST_PERCENTAGE_AMOUNT;

	double eta_w = ETA_W;
	double eta_b = ETA_B;

    while (begin)
    {
std::cout << i << "\n";

        z1 = feedforward(NN.L1, layerCount, input.rows(1,input.n_rows-1));
	//	std::cout << output1 << "\n\n";

		z2 = feedforward(NN.L2, layerCount, input.rows(1, input.n_rows - 1));
       
		z3 = NN.input_W.Wu * NN.input_W.W1 * z1 % input.row(0);
      //  y_final = NN.input_W.W1_ * z3 + NN.input_W.W2_ * input.row(0) ;
		y_final = z3 * NN.input_W.Wo + z2 * NN.input_W.W2;

        err = target - y_final;


		//definicao da funcao custo
		NN.it_cost.J_train.emplace_back(as_scalar(err.cols(0, trainSetSize - 1)
			* arma::trans(err.cols(0, trainSetSize - 1))) / (double) trainSetSize);

		NN.it_cost.J_val.emplace_back(as_scalar(err.cols(trainSetSize, trainSetSize + valSetSize - 1)
			* arma::trans(err.cols(trainSetSize, trainSetSize + valSetSize - 1))) / (double) valSetSize);

		NN.it_cost.J_test.emplace_back(as_scalar(err.cols(trainSetSize + valSetSize, err.n_cols - 1)
			* arma::trans(err.cols(trainSetSize + valSetSize, err.n_cols - 1))) / (double) testSetSize);

	//	std::cout << NN.L1[layerCount].dZ << "\n\n";

		if (NN.it_cost.J_val[i] < Jmin)
		{
			trained_network = NN;
			Jmin = NN.it_cost.J_val[i];
			trained_network.trainedOutput = y_final;
		}

		if (NN.it_cost.J_val[i] > 1000 || NN.it_cost.J_test[i] > 1000 ||
			NN.it_cost.J_train[i] > 1000)
		{
		//	trained_network = NN;
			trained_network.it_cost = NN.it_cost;
			begin = 0;
			cout << "exploded"<< endl;
		}

		if (i > 1)
		{
			if ( abs(NN.it_cost.J_val[i] - NN.it_cost.J_val[i - 1]) <= DELTA )
			{
				trained_network = NN;
				Jmin = NN.it_cost.J_val[i];
				trained_network.trainedOutput = y_final;
				cout << "converged" << endl;
				begin = 0;
			}
		}


		//definicao dos deltasW dos pesos externos
		mat err_train = err.cols(0, trainSetSize - 1);

        mat dWo = (-1)*(err_train) % z3.cols(0, trainSetSize - 1);

        mat dWu = (-1) * NN.input_W.Wo * NN.input_W.W1 *
			(err_train) % input.submat(0,0,0,trainSetSize-1) % z1.cols(0,trainSetSize-1);

		mat dW1 = (-1) * NN.input_W.Wo * NN.input_W.Wu *
			(err_train) % input.submat(0,0,0,trainSetSize-1) % z1.cols(0, trainSetSize-1);

		mat dW2 = (-1)*(err_train) % z2.cols(0, trainSetSize - 1);

		//definicao dos deltaW dos pesos internos
		//Layer de cima L1
		NN.L1[layerCount_1 + 1].alpha = NN.input_W.W1 * NN.input_W.Wo * NN.input_W.Wu * 
			input.submat(0, 0, 0, trainSetSize - 1) % err_train;
		NN.L1[layerCount_1 + 1].weight = 1;

     for (int32_t n=layerCount_1; n >= 0; n--)
        {
		 NN.L1[n].alpha = (NN.L1[n].dZ.cols(0, trainSetSize - 1)) % ((arma::trans(NN.L1[n + 1].weight)) * NN.L1[n + 1].alpha);
		 NN.L1[n].dB = arma::sum(NN.L1[n].alpha, 1) ;
		 NN.L1[n].dW = NN.L1[n].alpha*arma::trans(NN.L1[n].input.cols(0, trainSetSize - 1)) ;
        }

	 //Layer de baixo L2
	 NN.L2[layerCount_2 + 1].alpha = err_train * NN.input_W.W2  ;
	 NN.L2[layerCount_2 + 1].weight = 1 ;

	 for (int32_t n = layerCount_1; n >= 0; n--)
	 {
		 NN.L2[n].alpha = (NN.L2[n].dZ.cols(0, trainSetSize - 1)) % ((arma::trans(NN.L2[n + 1].weight)) * NN.L2[n + 1].alpha);
		 NN.L2[n].dB = arma::sum(NN.L2[n].alpha, 1);
		 NN.L2[n].dW = NN.L2[n].alpha*arma::trans(NN.L2[n].input.cols(0, trainSetSize - 1));
	 }
	 
	 
	  //iRprop_minus(NN.L1, layerCount_1);
	  //iRprop_minus(NN.L2, layerCount_2);
	 
	 ResilientBackProp_caloba(NN.L1, layerCount_1, a_);
	 ResilientBackProp_caloba(NN.L2, layerCount_2, a_);


#ifdef DEBUG
		std::cout <<"alpha l2 0 \n\n" << nn.l2[0].alpha << "\n\n" << nn.l2[1].alpha << "\n\n";
		std::cout << "alpha l2 0 \n\n" << nn.l2[layercount_2 + 1].alpha;
		std::cout << "\n\n" << nn.l2[layercount_2].alpha << "\n\n";


		cout << "\n\n\n\n\n\n\n\n\n\n\n";

		std::cout <<  err_train % output1_train * nn.input_w.w1_ * nn.input_w.w1 * nn.input_w.w2 << "\n\n";
		std::cout << err_train;
		std::cout << output1_train;
#endif



		//Atualizacoes
        for (int n = 0; n <= layerCount; n++)
        { 

          NN.L1[n].weight = NN.L1[n].weight + (double) eta_w*NN.L1[n].dW/ (double)trainSetSize;
          NN.L1[n].bias = NN.L1[n].bias + (double) eta_b*NN.L1[n].dB/ (double)trainSetSize;
          
          NN.L2[n].weight = NN.L2[n].weight + (double) eta_w*NN.L2[n].dW/ (double) trainSetSize;
          NN.L2[n].bias = NN.L2[n].bias + (double) eta_b*NN.L2[n].dB/ (double) trainSetSize;

          NN.L1[n].dW_Old = (double) eta_w*NN.L1[n].dW / (double) trainSetSize;
          NN.L1[n].dB_Old = (double) eta_b*NN.L1[n].dB / (double) trainSetSize;

          NN.L2[n].dW_Old = (double) eta_w*NN.L2[n].dW / (double) trainSetSize;
          NN.L2[n].dB_Old = (double) eta_b*NN.L2[n].dB / (double) trainSetSize;
        }

		NN.input_W.Wo = NN.input_W.Wo - (double) eta_w * arma::accu(dWo) / (double)trainSetSize;
		NN.input_W.Wu = NN.input_W.Wu - (double) eta_w * arma::accu(dWu) / (double)trainSetSize;

		NN.input_W.W1 = NN.input_W.W1 - (double) eta_w * arma::accu(dW1) / (double)trainSetSize;
		NN.input_W.W2 = NN.input_W.W2 - (double) eta_w * arma::accu(dW2) / (double)trainSetSize;

		if (i == runs-1) 
		{
			std::cout << "minimum validation cost: "
				<< Jmin << "\n";
			begin = false;
		}
		else
		{
			i++;
			eta_b *= BETA;
			eta_w *= BETA;
		}

    }

	trained_network.input_W.Wg = NN.input_W.W1 * NN.input_W.Wu * NN.input_W.Wo;
	trained_network.it_cost = NN.it_cost;
	return trained_network;
}

double trainResBPNarmaCONTROL(Network& NN,
	uint32_t layerCount,
	double d_output_error, 
	double& last_g, double& last_f, double& last_ref,	
	arma::mat& input, double target)
{

#ifdef NN_TRAIN

	uint32_t layerCount_1 = layerCount;
	uint32_t layerCount_2 = layerCount;

	double g_output = feedforwardControl(NN.L1, layerCount, input);
	//	std::cout << output1 << "\n\n";

	double f_output = feedforwardControl(NN.L2, layerCount, input);

	NN.L1[layerCount_1 + 1].alpha = -d_output_error * (last_f * NN.input_W.W2 - last_ref) / (last_g*last_g*NN.input_W.Wg);
	NN.L1[layerCount_1 + 1].weight = 1;

	for (int32_t n = layerCount_1; n >= 0; n--)
	{
		NN.L1[n].alpha = (NN.L1[n].dZ.col(0)) % ((arma::trans(NN.L1[n + 1].weight)) * NN.L1[n + 1].alpha);
		NN.L1[n].dB = arma::sum(NN.L1[n].alpha, 1);
		NN.L1[n].dW = NN.L1[n].alpha*arma::trans(NN.L1[n].input.col(0));
	}

	//Layer de baixo L2
	NN.L2[layerCount_2 + 1].alpha = -d_output_error * NN.input_W.W2 / (NN.input_W.Wg * last_g);
	NN.L2[layerCount_2 + 1].weight = 1;

	for (int32_t n = layerCount_1; n >= 0; n--)
	{
		NN.L2[n].alpha = (NN.L2[n].dZ.col(0)) % ((arma::trans(NN.L2[n + 1].weight)) * NN.L2[n + 1].alpha);
		NN.L2[n].dB = arma::sum(NN.L2[n].alpha, 1);
		NN.L2[n].dW = NN.L2[n].alpha*arma::trans(NN.L2[n].input.col(0));
	}

#endif

#ifdef WEIGTH_TRAIN
	
	NN.input_W.W2 += last_f / (last_g * NN.input_W.Wg * NN.input_W.Wg);
	NN.input_W.Wg += (last_ref - last_f *NN.input_W.W2) / (last_g * NN.input_W.Wg * NN.input_W.Wg);

#endif


	g_output = feedforwardControl(NN.L1, layerCount, input);
	//	std::cout << output1 << "\n\n";

	f_output = feedforwardControl(NN.L2, layerCount, input);

	
	double output_control = (target - f_output*NN.input_W.W2) / (NN.input_W.Wg * (g_output==0 ? 1 : g_output));

	last_f = f_output;
	last_g = g_output;
	last_ref = target;
	

	return output_control;

}