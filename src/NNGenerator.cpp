// Automatically translated using Matlab2cpp 0.5 on 2016-12-20 22:06:13
#include "header\stdafx.h"
#include "header\nngenerator.h"
#include <armadillo>

using namespace arma ;


std::vector<Layers> NNGenerator(uint32_t* neurons, uint32_t layerCount, double span, mat input, mat target)
{
	arma_rng::set_seed_random();
  std::vector<Layers> outputLayer;
  Layers aux;

  aux.weight.randu(neurons[0], input.n_rows-1);
  aux.weight = -span + 2 * span * aux.weight;
  
  aux.bias.randu(neurons[0], 1);
  aux.bias = -span + 2 * span *aux.bias;

  outputLayer.push_back(aux);

  for (uint32_t n=1; n < layerCount; n++)
  {

	aux.weight.randu(neurons[n], neurons[n - 1]);
	aux.weight = -span + 2 * span *aux.weight;

	aux.bias.randu(neurons[n], 1);
	aux.bias = -span + 2 * span *aux.bias;

	outputLayer.push_back(aux);
  }

  aux.weight.randu(target.n_rows, neurons[layerCount - 1]);
  aux.weight = -span + 2 * span *aux.weight;

  aux.bias.randu(target.n_rows, 1);
  aux.bias = -span + 2 * span *aux.bias;
  
  outputLayer.push_back(aux);
  outputLayer.push_back(aux);
  
  for (uint32_t n=0; n <= layerCount+1; n++)
  {
	  outputLayer[n].alpha = arma::zeros<mat>(target.n_cols) ;
	  outputLayer[n].dB = arma::zeros<mat>(size(outputLayer[n].bias)) ;
	  outputLayer[n].dW = arma::zeros<mat>(size(outputLayer[n].weight)) ;
	  outputLayer[n].dB_Old = outputLayer[n].dB;
	  outputLayer[n].dW_Old = outputLayer[n].dW;
	  outputLayer[n].Y = arma::zeros<mat>(outputLayer[n].weight.n_rows, target.n_cols);
	  outputLayer[n].Z = outputLayer[n].Y;
	  outputLayer[n].dZ = outputLayer[n].Y;
	  outputLayer[n].deltaB = span * arma::ones<mat>(size(outputLayer[n].dB));
	  outputLayer[n].deltaW = span * arma::ones<mat>(size(outputLayer[n].dW));
  }



  return outputLayer;
}