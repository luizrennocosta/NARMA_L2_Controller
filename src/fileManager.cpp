#include "header\fileManager.h"


std::vector<double> createDataSets(arma::mat &input, arma::mat &target, uint32_t i_delay, uint32_t t_delay) {
	arma::mat zero_matrix;
	zero_matrix.zeros(1);

	int32_t delay_target_min = 10;

	std::vector<double> norm_data;
	
	norm_data.push_back(arma::mean(arma::mean(input.col(0), 0)));
	norm_data.push_back(arma::stddev(input.col(0), 0));

	input.col(0) = (input.col(0) - arma::mean(arma::mean(input.col(0), 0))) / arma::stddev(input.col(0), 0);
	arma::mat aux = input;

	for (size_t i = 1; i <= i_delay; i++)
	{
		aux = arma::join_vert(zero_matrix, aux.rows(0, aux.n_rows - 2));
		input = arma::join_horiz(input, aux);

		//	input.col(i) = (input.col(i) - arma::mean(arma::mean(input.col(i), 0))) / arma::stddev(input.col(i), 0);
	}

	target.col(0) = (target.col(0) - arma::mean(arma::mean(target.col(0), 0))) / arma::stddev(target.col(0), 0);
	aux = target;

	target = arma::join_vert(target.rows(1, target.n_rows - 1), zero_matrix);

	for (size_t i = 1; i <= t_delay; i++)
	{
		aux = arma::join_vert(zero_matrix.zeros(delay_target_min, 1), aux.rows(0, aux.n_rows - delay_target_min - 1));
		if (i >= 2)
		{
			input = arma::join_horiz(input, aux);
			//input.col(i_delay + i - 1) = (input.col(i_delay + i - 1)- arma::mean(arma::mean(input.col(i_delay + i - 1), 0))) / arma::stddev(input.col(i_delay + i - 1), 0);
		}
	}
	return norm_data;
}


void readFileNew(std::string absoluteFilePath,
	arma::mat& input_arma, arma::mat& target_arma, uint32_t downsample) 
{
	std::vector<double> input, output;

	std::ifstream file_stream(absoluteFilePath);

	if (file_stream.is_open()) {

		std::string line;
		std::getline(file_stream, line);
		std::getline(file_stream, line);
		std::getline(file_stream, line);
		uint32_t counter = 0;
		while (std::getline(file_stream, line))
		{
			std::istringstream ss(line);

			// possibly you will want some other types here.
			double biceps, triceps, angle, dummy, current;

			if (counter >= 500) {
				if (counter % downsample == 0){
					ss >> angle;
					ss >> dummy;
					ss >> dummy;
					ss >> dummy;
					ss >> biceps;
					ss >> triceps;

					current = biceps - triceps;	

					input.push_back(current);
					output.push_back(angle);
				}
			}
			counter++;
		}
	}
	else {
		std::cout << "Error opening File \n";
	}

	input_arma = arma::conv_to<arma::mat>::from(input);
	target_arma = arma::conv_to<arma::mat>::from(output);

}