// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#include "nntypes.h"

Network labview_to_armadillo(double* NN_Weights_L1, double* NN_Weights_L2, double* input_weights, uint32_t* w_dim, uint32_t layerCount);

Interface_Sizes armadillo_to_labview(Network NN, double** NN_Weights_L1, double** NN_Weights_L2, double* input_weights, uint32_t** w_dim);