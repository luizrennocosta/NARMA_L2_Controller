#ifndef TRAIN_RES_BP_NARMA_H
#define TRAIN_RES_BP_NARMA_H

#include "nntypes.h"
#include "feedforward.h"
#include "resilientbackprop.h"

#define DELTA	0.000001

Network trainResBPNARMAL2(Network NN,
	uint32_t layerCount,
	arma::mat input, arma::mat target,
	uint32_t runs);

double trainResBPNarmaCONTROL(Network& NN,
	uint32_t layerCount,
	double d_output_error,
	double& last_g, double& last_f, double& last_ref,
	arma::mat& input, double target);

#endif 