#ifndef NNGENERATOR_H
#define NNGENERATOR_H

#include "nntypes.h"


std::vector<Layers> NNGenerator(uint32_t* neurons, uint32_t layerCount, double span, arma::mat input, arma::mat target);

#endif