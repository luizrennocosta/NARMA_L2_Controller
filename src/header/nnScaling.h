#ifndef NN_SCALING_H
#define NN_SCALING_H

#include "nntypes.h"

// o vetor means consiste do vetor de medias de todas entradas e A UNICA SAIDA.
// ex: caso tenhamos u1 u2 u3 y1 -> means = m1 m2 m3 m_y1
// o vetor stds [e analogo

void nnInputScaling(std::vector<Layers>& L, std::vector<double> means, std::vector<double> stds);

void narmaNetworkScaling(Network& narma, std::vector<double> means, std::vector<double> stds);

#endif