#ifndef RES_BPROP_H
#define RES_BPROP_H

#include "nntypes.h"

#define ETA_PLUS 1.2
#define ETA_MINUS 0.5

#define DELTA_MIN 0.1
#define DELTA_MAX 200

void ResilientBackProp_caloba(std::vector<Layers> &L, uint32_t layerCount, double &a_);

// http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.21.3428&rep=rep1&type=pdf
void iRprop_minus(std::vector<Layers> &L, uint32_t layerCount);

#endif