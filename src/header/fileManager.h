#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include "nntypes.h"
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

std::vector<double> createDataSets(arma::mat& input_arma, arma::mat& target_arma,
	uint32_t i_delay, uint32_t t_delay);

void readFileNew(std::string absoluteFilePath,
	arma::mat& input_arma, arma::mat& target_arma, uint32_t downsample);

#endif