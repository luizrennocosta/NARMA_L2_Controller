#ifndef NNTYPES_H
#define NNTYPES_H

//file that holds all defines and variable structures used.

#include <armadillo>
#include <vector>

#define MAXIMUM_A_VALUE 1 // maximum value for the resilient backpropagation 
#define A_FACTOR_RES_BP	1

#define ETA_W	0.3
#define ETA_B	ETA_W
#define BETA	0.999
#define SPAN	1.5

#define TRAIN_PERCENTAGE_AMOUNT			0.7f
#define VALIDATION_PERCENTAGE_AMOUNT	0.15f
#define TEST_PERCENTAGE_AMOUNT			0.15f

struct Layers
{
	arma::Col<uint32_t> neurons;
	arma::mat input, Y, Z, dZ;
	arma::mat alpha;
	arma::mat weight, bias;
	arma::mat dB, dW;
	arma::mat dB_Old, dW_Old;
	arma::mat deltaW, deltaB;
};


struct input_weights {
	double W1, W2, Wu, Wo, Wg;
};

struct cost {

	std::vector<double> J_train;
	std::vector<double> J_val;
	std::vector<double> J_test;

};

struct Network {
	std::vector<Layers> L1;
	std::vector<Layers> L2;
	input_weights input_W;
	cost it_cost;
	uint32_t layerCount;
	arma::mat trainedOutput;
	uint32_t input_delay;
	uint32_t output_delay;

};

struct Interface_Sizes {
	uint32_t W1_size;
	uint32_t dim_size;
};

#endif