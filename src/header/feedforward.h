// Automatically translated using Matlab2cpp 0.5 on 2016-12-17 14:15:37
#ifndef FEEDFORWARD_H
#define FEEDFORWARD_H

#include "nntypes.h"

// inner_activation = tgh
// outter_activation = linear

arma::mat feedforward(std::vector<Layers> &L, uint32_t layerCount, arma::mat input);

double feedforwardControl(std::vector<Layers> &L, uint32_t layerCount, arma::mat input);

#endif
