// Automatically translated using Matlab2cpp 0.5 on 2016-12-17 14:15:37
			
#include "header\feedforward.h"

// inner_activation = tgh
// outter_activation = linear

arma::mat feedforward (std::vector<Layers> &L, uint32_t layerCount, arma::mat input)
{

	arma::mat output;
	L[0].input = input;

	for (int n=0; n < layerCount; n++)
	{

		L[n].Y = (L[n].weight)*L[n].input + repmat(L[n].bias, 1, input.n_cols);


		L[n].Z = tanh(L[n].Y);
		L[n].dZ = (1 - L[n].Z % L[n].Z);

		L[n+1].input = L[n].Z;

	/*	std::cout << L[n].weight << "\n\n\n\n------------------------------";
		std::cout << L[n].bias << "\n\n\n\n------------------------------";
		std::cout << L[n].Y << "\n\n\n\n------------------------------";
		std::cout << L[n].Z << "\n\n\n\n------------------------------";
		std::cout << L[n].dZ << "\n\n\n\n------------------------------";
		std::cout << L[n].input << "\n\n\n\n------------------------------";*/
	}

	L[layerCount].Y = (L[layerCount].weight)*L[layerCount].input + repmat(L[layerCount].bias, 1, input.n_cols);
	L[layerCount].Z = L[layerCount].Y;

	//std::cout << L[layerCount].Z << "\n\n\n\n";

	L[layerCount].dZ.ones(1,input.n_cols);

	// L[n + 1].input = L[n].Z;
	output = L[layerCount].Z;

	return output;
}

double feedforwardControl(std::vector<Layers>& L, uint32_t layerCount, arma::mat input)
{
	arma::mat output;
	L[0].input = input;

	for (int n = 0; n < layerCount; n++)
	{

		L[n].Y = (L[n].weight)*L[n].input + repmat(L[n].bias, 1, input.n_cols);

		std::cout << L[n].weight << std::endl << std::endl;
		std::cout << L[n].bias << std::endl << std::endl;
		std::cout << L[n].input << std::endl << std::endl;
		std::cout << std::endl << std::endl << std::endl;



		L[n].Z = tanh(L[n].Y);
		L[n].dZ = (1 - L[n].Z % L[n].Z);

		std::cout << L[n].Y << std::endl << std::endl;
		std::cout << L[n].Z << std::endl << std::endl;
		std::cout << L[n].dZ << std::endl << std::endl;

		L[n + 1].input = L[n].Z;

		/*	std::cout << L[n].weight << "\n\n\n\n------------------------------";
		std::cout << L[n].bias << "\n\n\n\n------------------------------";
		std::cout << L[n].Y << "\n\n\n\n------------------------------";
		std::cout << L[n].Z << "\n\n\n\n------------------------------";
		std::cout << L[n].dZ << "\n\n\n\n------------------------------";
		std::cout << L[n].input << "\n\n\n\n------------------------------";*/
	}

	L[layerCount].Y = (L[layerCount].weight)*L[layerCount].input + repmat(L[layerCount].bias, 1, input.n_cols);
	L[layerCount].Z = L[layerCount].Y;

	std::cout << L[layerCount].weight << std::endl << std::endl;
	std::cout << L[layerCount].bias << std::endl << std::endl;
	std::cout << L[layerCount].input << std::endl << std::endl;
	std::cout << std::endl << std::endl << std::endl;

	std::cout << L[layerCount].Y << std::endl << std::endl;
	std::cout << L[layerCount].Z << std::endl << std::endl;

	//std::cout << L[layerCount].Z << "\n\n\n\n";

	L[layerCount].dZ = 1;

	// L[n + 1].input = L[n].Z;
	output = L[layerCount].Z;

	return arma::as_scalar(output);
}
