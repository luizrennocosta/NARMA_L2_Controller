// narmadll.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "nntypes.h"
#include "fileManager.h"
#include "nngenerator.h"
#include "trainResBPNarma.h"
#include "labviewInterface.h"
#include "nnScaling.h"

//typedef struct {
//	int32_t dimSize;
//	double arg1[1];
//} TD1;
//typedef TD1 **TD1Hdl;

double __declspec(dllexport) labview_initialize(uint32_t* neurons, uint32_t layerCount, uint32_t input_delay, uint32_t output_delay, double span, uint32_t downsample, char* file_path,
	uint32_t runs, double* weights_L1,double* weights_L2, double* input_W_test, uint32_t* dimensions)

{
	std::string file_path_str(file_path);
	arma::mat input_arma, target_arma;
	std::vector<double> norm_data;
	std::vector<double> means_vec, std_vec;
	arma::mat aux;
	input_weights i_W;
	Interface_Sizes labview_it;

	double* dummy_w1 = NULL;
	double* dummy_w2 = NULL;
	uint32_t* dummy_dim = NULL;

	readFileNew(file_path_str, input_arma, target_arma, downsample);

	norm_data = createDataSets(input_arma, target_arma, input_delay, output_delay);

	input_arma = input_arma.t();
	target_arma = target_arma.t();

	std::vector<Layers> L1, L2;
	arma::mat aux_ = input_arma.row(0);


	L1 = NNGenerator(neurons, layerCount, span, input_arma, target_arma);
	L2 = NNGenerator(neurons, layerCount, span, input_arma, target_arma);

	//structure that holds the other input weights
	aux.randu(1, 4);
	aux = -span + 2 * span* aux;
	i_W.W1 = aux[0];
	i_W.W2 = aux[1];
	i_W.Wo = aux[2];
	i_W.Wu = aux[3];


	Network neural_network_NARMA;
	neural_network_NARMA.layerCount = layerCount;

	neural_network_NARMA.L1 = L1;
	neural_network_NARMA.L2 = L2;
	neural_network_NARMA.input_W = i_W;

	neural_network_NARMA.it_cost.J_train.reserve(runs);
	neural_network_NARMA.it_cost.J_val.reserve(runs);
	neural_network_NARMA.it_cost.J_test.reserve(runs);

	//training
	neural_network_NARMA = trainResBPNARMAL2(neural_network_NARMA,
		layerCount,
		input_arma, target_arma,
		runs);

	for (size_t i = 0; i < input_delay + output_delay - 1; i++)
	{
		means_vec.push_back(norm_data[0]);
		std_vec.push_back(norm_data[1]);
	}

	narmaNetworkScaling(neural_network_NARMA, means_vec, std_vec);

	labview_it = armadillo_to_labview(neural_network_NARMA, &dummy_w1, &dummy_w2, input_W_test, &dummy_dim);

	for (size_t i = 0; i < labview_it.W1_size; i++)
	{
		weights_L1[i] = dummy_w1[i];
		weights_L2[i] = dummy_w2[i];
	}

	for (size_t i = 0; i < labview_it.dim_size; i++)
	{
		dimensions[i] = dummy_dim[i];
	}

	return (means_vec.back() - input_W_test[3] * input_W_test[2] * input_W_test[0] * means_vec[0]);
}


double __declspec(dllexport) operateControl(double* weights_L1, double* weights_L2, double* input_W_test, uint32_t* dimensions, uint32_t layerCount, 
	double d_output_error, double* last_elements, double* input_labview, double target, uint32_t input_delay, uint32_t output_delay)
{
	Network NN;
	double* dummy_w1 = NULL;
	double* dummy_w2 = NULL;
	uint32_t* dummy_dim = NULL;
	Interface_Sizes labview_it;
	double output_control;
	arma::mat input_arma(input_labview, input_delay + output_delay - 1, 1);

	double last_g = last_elements[0];
	double last_f = last_elements[1];
	double last_ref = last_elements[2];


	NN = labview_to_armadillo(weights_L1, weights_L2, input_W_test, dimensions, layerCount);

	output_control = trainResBPNarmaCONTROL(NN,
		layerCount,
		d_output_error,
		last_g, last_f, last_ref,
		input_arma, target);


	labview_it = armadillo_to_labview(NN, &dummy_w1, &dummy_w2, input_W_test, &dummy_dim);


	for (size_t i = 0; i < labview_it.W1_size; i++)
	{
		weights_L1[i] = dummy_w1[i];
		weights_L2[i] = dummy_w2[i];
	}

	for (size_t i = 0; i < labview_it.dim_size; i++)
	{
		dimensions[i] = dummy_dim[i];
	}

	return output_control;
}

void __declspec(dllexport) data_delayer(double input, double output, double* delayed_data, uint32_t input_delay, uint32_t output_delay)
{
	for (size_t i = input_delay - 1; i > 0; i--)
	{
		delayed_data[i] = delayed_data[i - 1];
	}

	delayed_data[0] = input;

	for (size_t i = input_delay + output_delay - 1 - 1; i > input_delay; i--)
	{
		delayed_data[i] = delayed_data[i - 1];
	}

	delayed_data[input_delay] = output;
}